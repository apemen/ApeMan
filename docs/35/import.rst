:orphan:

Import in 3.5
=============

This is meant to discuss nuances in Python 3.5 but they are not listed just yet.
 
.. note :: API Changes

 This note is a copy of one from a prior section and should be removed at some point ``imp.find_module`` -> ``imp.load_module``


