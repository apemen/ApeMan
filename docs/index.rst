.. ApeMan documentation master file, created by sphinx-quickstart on Wed Nov 30 23:32:50 2016.
   You can adapt this file completely to your liking, but it should at least contain the root `toctree` directive.

==================
Portage for Python
==================

.. toctree::
   :maxdepth: 3
   :hidden:

   Home Page                  <self>
   Objective                  <objective>
   Terminology                <terminology>
   Mechanism                  <import>
   Machinery                  <machinery>
   Observations               <observations>
  
   ApeMan                     <apeman>
  
   Overlays                   <overlay>
  
   Literature                 <literature>
   Contribution               <contribution>
   Testing                    <tests>

   Frequently Asked Questions <faq>
   Glossary                   <glossary>

The cursory reader need only cover the contents upon this, the main/home page, to understand why and how one might use ApeMan.
Those wanting an introduction into how ApeMan works should read the :ref:`objective:Objective` and :ref:`import:Python's Import Mechanism` sections.
While those looking into it's implementation should read the :ref:`apeman:ApeMan`, :ref:`machinery:Machinery`, :ref:`tests:Testing` and  :ref:`contribution:Contribution` sections.
:ref:`machinery:Machinery` and :ref:`literature:Literature` discuss and provide resources for those interested in the Python import system.
The remainder of the documenation reviews the Python import system and dicusses the implementation of ApeMan within it.
   
.. only :: builder_html

   .. include :: ../readme.rst

------------------
Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

