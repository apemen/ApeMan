"""
Machinery
=========

These tests represent examples of how the underlying machinery works and how to use it properly.
They are also useful as a suite of sanity checks. 

.. note ::

   This suite was originally called :mod:`testAssumptions`. 
   It has since been renamed and the original content refactored into the documentation under :ref:`Diagnostics`.
   
   
.. todo ::

   Explore the use of Mocks to interrogate the import system's behaviour.
"""
# Python Compatability
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import six
# Testing
import unittest
# System
import sys
# Imp
if six.PY2 : 
 pass
# Importlib
if six.PY3 : 
 from importlib import machinery, util
# Operating System
import os
from pathlib import Path
# Pretty Printing
from pprint import pprint

@unittest.skipIf(six.PY2,"Importlib only shipped with Python 3.4")
class testSourceFileLoader(unittest.TestCase) :
 """
 Load a module using the SourcefileLoader
 """
 # Machinery/           Mockup/
 #  NameSpace/           implicit/  
 #   NameSpace.py         module.py
 #  Package/             explicit/
 #   __init__.py          __init__.py
 #  NameSpace.py         module.py
 
 def setUp(self) :
  """
  Insert the mockup path into the system path list.
  """
  #: the path containing the mockup package
  self.path = Path(__file__).resolve().parents[1]/'mockup' 
  # Originally : os.path.join(os.path.dirname(os.path.abspath(__file__)),'machinery')
  sys.path.append(self.path)

 def tearDown(self) : 
  """
  Remove the mockup path from the system path list.
  """
  sys.path.pop()

 def testInitializeExplicitPackageInit(self) :
  """Tests that a package with an __init__ file is loadable given the complete path"""
  loader = machinery.SourceFileLoader("name",str((self.path/"explicit")/"__init__.py"))
  self.assertEqual(loader.load_module().__version__,(0,1),__doc__)
  
 def testInitializeExplicitPackagePath(self) :
  """Tests that a package with an __init__ file is loadable given the directory"""
  loader = machinery.SourceFileLoader("name",str(self.path/"explicit"))
  with self.assertRaises(PermissionError) :
   loader.load_module()
  
 def testInitializeImplicitPackagePath(self) :
  """Tests that a package with an __init__ file is loadable given the directory"""
  loader = machinery.SourceFileLoader("name",str(self.path/"implicit"))
  with self.assertRaises(PermissionError) :
   loader.load_module()
  
 def testInitializeModule(self) :
  """Tests the loading of a module via the SourceFileLoader"""
  loader = machinery.SourceFileLoader("module",str(self.path/"module.py"))
  self.assertEqual(str(loader.load_module().Class()),".module.ClassA") # Note the inclusion of the "name"
  
 def testInitializeImplicitModule(self) :
  """Tests the loading of a submodule via the SourceFileLoader"""
  # Tests that a module is directly importable
  loader = machinery.SourceFileLoader("implicit",str(self.path/"implicit"/"module.py"))
  self.assertEqual(str(loader.load_module().Class()),"implicit.ClassB")

 def testInitializeExplicitModule(self) :
  """Tests the loading of a submodule via the SourceFileLoader"""
  # Tests that a module is directly importable
  loader = machinery.SourceFileLoader("explicit",str(self.path/"explicit"/"module.py"))
  self.assertEqual(str(loader.load_module().Class()),"explicit.ClassB")

@unittest.skipIf(six.PY2,"Importlib only shipped with Python 3.4")
class testModuleSpecWithSourceFileLoader(unittest.TestCase) :
 """
 These tests target the usage of ModuleSpecs to load modules.
 Specifically the Modulespec uses the SourceFileLoader to do the work.
 """
 
 def setUp(self) :
  """
  Insert the mockup path into the system path list.
  """
  #: The path containing the mockup package
  self.path = Path(__file__).resolve().parents[1]/'mockup' # Originally : os.path.join(os.path.dirname(os.path.abspath(__file__)),'machinery')
  sys.path.append(self.path)

 def tearDown(self) :
  """
  Remove the mockup path from the system path list.
  """
  sys.path.pop()
  
 def testSourceFileLoaderAsModuleSpecLoader(self) :
  """Tests that a package with an __init__ file is loadable given the complete path, using both a """
  loader = machinery.SourceFileLoader("module",str(self.path/"module.py")) # Originaly : explicit/__init__.py
  module = machinery.ModuleSpec("_test_", loader)
  self.assertEqual(str(module.loader.load_module().Class()),".module.ClassA")
  self.assertEqual(str(loader.load_module().Class()),".module.ClassA")

 def testModuleSpecFromFile(self) :
  """Tests that a package with an __init__ file is loadable given the complete path"""
  spec = util.spec_from_file_location("module",str(self.path/"module.py"))
  self.assertEqual(str(spec.loader.load_module().Class()),".module.ClassA")

if __name__ == '__main__':
 import sys
 import logging # Readup upon logging adapters,
 from pprint import pprint
 logging.basicConfig(format = '%(message)s') # level=logging.DEBUG)#, stream=sys.stdout)
 logger = logging.getLogger("layman.layman")
 logger.setLevel(logging.DEBUG)
#  target = logging.StreamHandler()
#  target.setLevel()
#  format = logging.Formatter('%(message)s')
#  target.setFormatter(format)
#  logger.addHandler(target)
#  pprint(dir(logger.root))
 try : 
  logfile = Path(__file__).with_suffix(".log")
  with open(str(logfile),'w') as log :
   runner = unittest.TextTestRunner(log)
   unittest.main(testRunner=runner)# , buffer=True, catchbreak=True)
 finally : 
  with open(str(logfile),'r') as log :
   [print(line.rstrip()) for line in log.readlines()]
  