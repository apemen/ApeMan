"""
Example test
============

Initially this was just an example test to illustrate the use of apeman with mock'd modules.
It is actually a fundamental test of both libraries in unison.
This should probably be moved into the mock test code.

This test mocks' a scaffold and the overlay structure on top of it.
Then it check various features of the import.

As it stands it needs to be refined somewhat since it doesn't provide any formal test cases.
Rather it serves more like an example and this needs to be coreected.
"""
import six
import logging
import sys
if six.PY2 :
 from unittest import main, TestCase
 from mock     import patch
if six.PY3 : 
 import overlays
 from unittest      import main, TestCase
 from unittest.mock import patch

 overlayCode = \
 """
 from apeman import ApeMan 
 ApeMan()
 """
 
 @patch("sys.meta_path", sys.meta_path)
 class testExample(TestCase) :
 
  #  @patch.module("overlay",          "overlay\\__init__.py", overlayCode)
  #  @patch("sys.modules", sys.modules)
  @patch.module("overlay._module_", "package\\_module_.py", """print('p._m_')""")
  @patch.module("module",           "module.py",            """print('p.m')""")
  @patch("sys.modules", sys.modules)
#   @unittest.skipIf(six.PY2, "Mock not supported in the ApeMan Overlays just yet")
  def test() :
  #  import module
   import overlay._module_
   
 #   import overlay
 #   import module
 #   import overlay._module_

if __name__ == "__main__" :
 unittest.main()
